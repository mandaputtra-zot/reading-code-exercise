class LRUCache {
  max: number;
  cache: Map<string, number>

  constructor(max = 10) {
    this.max = max;
    this.cache = new Map();
  }

  get(key: string) {
    let item = this.cache.get(key);
    if (item) {
      this.cache.delete(key);
      this.cache.set(key, item);
    }
    return item;
  }

  set(key: string, val: number) {
    if (this.cache.has(key)) {
      this.cache.delete(key);
    } else if (this.cache.size == this.max) {
      this.cache.delete(this.first());
    } 
    this.cache.set(key, val);
  }

  first() {
    return this.cache.keys().next().value;
  }
}

const lru = new LRUCache(3)

lru.set('a', 1)
lru.set('b', 1)
lru.set('c', 1)

lru.get('b')

lru.set('d', 1)
