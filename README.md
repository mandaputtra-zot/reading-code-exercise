# Reading Code Exercise

There are 3 files that contain some of finished code, try to read it and guess what are the outcome of the code written. Try to reason how it works, and maybe if you can do ~ improve it.

Start with `basics.ts`, `queue.ts`, and then `simple-lru.ts`.

## Running The Script

```bash
$ npm install
```

Run per file

```bash
$ npx ts-node <your typescript file.ts>
```
