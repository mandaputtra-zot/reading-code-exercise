type QueElements<T> = Record<number, T>

function Queue<T>() {
  let elements: QueElements<T> = {}
  let head = 0;
  let tail = 0;

  function enqueue(el: T) {
    elements[tail] = el
    tail++
  }

  function dequeue() {
    const item = elements[head]
    delete elements[head]
    head++
    return item
  }

  function peek() {
    return elements[head]
  }

  function length() {
    return tail - head
  }

  function isEmpty() {
    return length() === 0
  }

  return { enqueue, dequeue, peek, length, isEmpty }
}

// Run

const que = Queue<number>()

console.log(que.isEmpty())

que.enqueue(10)
que.enqueue(1)
que.enqueue(9)
que.enqueue(1)
que.enqueue(112)
que.enqueue(11)

que.dequeue()
que.dequeue()
que.dequeue()

console.log(que.peek())

console.log(que.length())
