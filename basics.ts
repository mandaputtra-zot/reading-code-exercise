// Javascript Copying Object what would be the output?
// console.log(copyOverObject())
export function copyOverObject() {
  const personOne = { name: 'takat', address: 'jakarta' }
  const normalAssign = personOne
  normalAssign.name = 'talis'
  normalAssign.address = 'semarang'

  const personTwo = { name: 'takat', address: 'jakarta' }
  const copyObject = {...personTwo}
  copyObject.name = 'talis'
  copyObject.address = 'semarang'

  return {
    normalAssign: personOne,
    withCopyAssign: personTwo,
  }
}

// String counter, what would be the output?
export function stringCounter(s: string) {
  const stringCounts: Record<string, number> = {};
  const countOccurence: Record<string, number> = {};

  for (let i = 0; i < s.length; i++) {
    const letter = s[i];
    stringCounts[letter] = (stringCounts[letter] || 0) + 1;
  }

  for (const key in stringCounts) {
    const letterCount = stringCounts[key];
    countOccurence[letterCount] = (countOccurence[letterCount] || 0) + 1;
  }

  return { stringCounts, countOccurence }
}

// console.log(isValidString('aabbc'))
// console.log(isValidString('abcddd'))

// Do you know how to use this function?
export const pipe = <T>(...fns: Array<(arg: T) => T>) => (value: T) => fns.reduce((acc, fn) => fn(acc), value);

// Promises, why does this function writtern like this?
function cbPromises<T, R>(params: T, callback?: (a: T | null, error?: Error) => R) {
  const error = new Error(`${params} Not A Number`)
  if (typeof callback === 'function') {
    if (isNaN(Number(params))) return callback(null, error)
    return callback(params)
  }
  return new Promise((resolve, reject) => {
    if(isNaN(Number(params))) reject(error)
    resolve(params)
  })
}

// Promises, what will the output be?
const promises: Array<Promise<unknown>> = []

function sleep(i: number) {
  return new Promise((resolve, _) => {
    console.log(`function called ${i}`)
    return setTimeout(() => {
      resolve(console.log(`function resolved ${i}`))
    }, 1000)
  })
}

for (let i = 0; i < 10; i++) {
  promises.push(
    sleep(i)
  )
}

// which code run faster?
(async () => {
  console.time('sequential')
  await sleep(1)
  await sleep(2)
  console.timeEnd('sequential')

  console.time('concurrent')
  await Promise.all(promises)
  console.timeEnd('concurrent')
})()
